<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Category;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\AlertServiceInterface;

#[Route('/article')]
class ArticleController extends AbstractController
{

     /**
     * @var AlertServiceInterface
     */
    private AlertServiceInterface $alertService;

    /**
     * @param AlertServiceInterface $alertService
     */
    public function __construct(AlertServiceInterface $alertService)
    {
        $this->alertService = $alertService;
    }

    #[Route('/', name: 'app_article_index', methods: ['GET'])]
    public function index(ArticleRepository $articleRepository,CategoryRepository $categoryRepository): Response
    {
        return $this->render('article/index.html.twig', [
            'articles' => $articleRepository->findAll(),
            'categories' => $categoryRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_article_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ArticleRepository $articleRepository, CategoryRepository $categoryRepository): Response
    {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $articleRepository->add($article);

            $this->alertService->success(sprintf('L\'article <b>"%s"</b> a été créer', $article->getTitle()));

            return $this->redirectToRoute('app_article_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('article/new.html.twig', [
            'article' => $article,
            'form' => $form,
            'categories' => $categoryRepository->findAll(),
        ]);
    }


    #[Route('/{slug}', name: 'app_article_show', methods: ['GET'])]
    public function show($slug,  EntityManagerInterface $entityManager, CategoryRepository $categoryRepository): Response
    {
        $article = $entityManager->getRepository(Article::class)->findOneById($slug);

        return $this->render('article/show.html.twig',[
            'article' => $article,
            'categories' => $categoryRepository->findAll(),
        ]);
    }


    #[Route('/{id}/edit', name: 'app_article_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Article $article, ArticleRepository $articleRepository, CategoryRepository $categoryRepository): Response
    {
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $articleRepository->add($article);

            $this->alertService->success(sprintf('L\'article <b>"%s"</b> a été modifié', $article->getTitle()));

            return $this->redirectToRoute('app_article_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('article/edit.html.twig', [
            'article' => $article,
            'form' => $form,
            'categories' => $categoryRepository->findAll(),
        ]);
    }

    #[Route('/{id}', name: 'app_article_delete', methods: ['POST'])]
    public function delete(Request $request, Article $article, ArticleRepository $articleRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$article->getId(), $request->request->get('_token'))) {
            $this->alertService->success(sprintf('L\'article <b>"%s"</b> a été supprimé', $article->getTitle()));
            $articleRepository->remove($article);
        }

        return $this->redirectToRoute('app_article_index', [], Response::HTTP_SEE_OTHER);
    }
}
