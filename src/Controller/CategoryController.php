<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Article;
use App\Form\CategoryType;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\AlertServiceInterface;


#[Route('/category')]
class CategoryController extends AbstractController
{

     /**
     * @var AlertServiceInterface
     */
    private AlertServiceInterface $alertService;

    /**
     * @param AlertServiceInterface $alertService
     */
    public function __construct(AlertServiceInterface $alertService)
    {
        $this->alertService = $alertService;
    }

    #[Route('/', name: 'app_category_index', methods: ['GET'])]
    public function index(CategoryRepository $categoryRepository): Response
    {
        return $this->render('category/index.html.twig', [
            'categories' => $categoryRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_category_new', methods: ['GET', 'POST'])]
    public function new(Request $request, CategoryRepository $categoryRepository): Response
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $categoryRepository->add($category);

            $this->alertService->success(sprintf('La catégorie <b>"%s"</b> a été créer', $category->getName()));

            return $this->redirectToRoute('app_category_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('category/new.html.twig', [
            'categories' => $category,
            'form' => $form,
        ]);
    }

    #[Route('/{slug}', name: 'app_category_show', methods: ['GET'])]
    public function show($slug,  EntityManagerInterface $entityManager, CategoryRepository $categoryRepository): Response
    {
        $category = $entityManager->getRepository(Category::class)->findOneById($slug);
        $idCategory = $category->getId();

        $articles = $entityManager->getRepository(Article::class)->findByCategory($idCategory);

        if(!$category){
            return $this->redirectToRoute('app_home');
        }

        return $this->render('category/list.html.twig',[
            'category'=> $category,
            'articles' => $articles,
            'categories' => $categoryRepository->findAll(),
        ]);
    }

    #[Route('/{id}/edit', name: 'app_category_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Category $category, CategoryRepository $categoryRepository): Response
    {
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $categoryRepository->add($category);

            $this->alertService->success(sprintf('La catégorie <b>"%s"</b> a été modifié', $category->getName()));

            return $this->redirectToRoute('app_category_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('category/edit.html.twig', [
            'categories' => $category,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_category_delete', methods: ['POST'])]
    public function delete(Request $request, Category $category, CategoryRepository $categoryRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$category->getId(), $request->request->get('_token'))) {

            $this->alertService->success(sprintf('La catégorie <b>"%s"</b> a été supprimé', $category->getName()));
            $categoryRepository->remove($category);
        }

        return $this->redirectToRoute('app_category_index', [], Response::HTTP_SEE_OTHER);
    }
}
