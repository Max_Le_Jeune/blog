<?php

namespace App\Form;

use App\Entity\Article;
use App\Entity\Category;
use App\Form\Type\SluggerType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add('title', TextType::class, [
            'label' => 'Nom'
        ])
        ->add('slug', SluggerType::class, [
            'name_class' => Article::getNameClass(),
            'field' => 'title',
        ])
        ->add('date', DateType::class, [
            'label' => 'Date de publication'
        ])
        ->add('content', TextareaType::class, [
            'label' => 'contenue',
            'required' => false
        ])
        ->add('category', EntityType::class, [
            'label' => 'Catégorie',
            'class' => Category::class,
            'required' => true,
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
