<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;

class RegisterType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'Votre Email',
                'constraints'=> new Length([
                    'min'=> 2,
                    'max'=> 60,
                ]),
                'attr' => [
                    'placeholder' => 'Saisir votre Email',
                    'class'=> 'form-control'
                ]
            ])
            ->add('password', RepeatedType::class, [
                'type'=> PasswordType::class,
                'invalid_message'=> 'Les 2 mots de passe ne sont pas similaire',
                'label' => 'Votre Mot de Passe',
                'required'=> true,
                'first_options' => [
                    'label'=>'Mot de passe',
                    'attr'=>[
                        'placeholder'=> 'Votre mot de passe',
                        'class'=> 'form-control'
                    ]
                ],
                'second_options' => [
                    'label'=>'Confirmez votre Mot de passe',
                    'attr'=>[
                        'placeholder'=> 'Confirmez le mot de passe',
                        'class'=> 'form-control'
                    ]
                ]
            ])
        ;
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
