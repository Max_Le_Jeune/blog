<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SluggerType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setRequired(['name_class', 'field'])
            ->setDefaults([
                'compound' => false,
            ])
            ->setAllowedTypes('name_class', ['string'])
            ->setAllowedTypes('field', ['string'])
        ;
    }

    /**
     * @inheritDoc
     */
    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $view->vars['attr']['data-target'] = mb_strtolower($options['name_class']) . '_' . $options['field'];
        $view->vars['attr']['class'] = 'slug-field';
    }

    /**
     * @inheritDoc
     */
    public function getParent(): string
    {
        return TextType::class;
    }
}
